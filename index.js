// Khởi tạo mảng.
var ArrN = [];

function ThemSoN(){
    console.log("Thêm số n vào mảng:");
    
    var n = document.getElementById("txtn").value * 1;
    document.getElementById("txtn").value = "";
    
    ArrN.push(n);
    console.log('Mảng n: ', ArrN);

    document.getElementById("MangN").innerHTML = 
    `<h2>Mảng n: ${ArrN}</h2>`

    console.log("----------------------");
}

// Bài 1: tính tổng số dương.
function TinhTongDuong(){
    var sum = 0;
    console.log("Bài 1 - Tính tổng số dương");
    for(var i = 0; i < ArrN.length; i++){
        var mtn = ArrN[i];
        if(mtn>0){
            sum += mtn;
        }
    }
    console.log('Tổng số dương: ', sum);
    document.getElementById("KQ_1").innerHTML = 
    `<h2>Tổng số dương: ${sum}</h2>`
    console.log("----------------------");
}

// Bài 2: Đếm số dương.
function DemSoDuong(){
    var dem = 0;
    console.log("Bài 2 - Đếm số dương");
    for(var i = 0; i < ArrN.length; i++){
        var mtn = ArrN[i];
        if(mtn > 0){
            dem ++;
        }
    }
    console.log('so dương: ', dem);
    document.getElementById("KQ_2").innerHTML = 
    `<h2>Số dương: ${dem}</h2>`
    console.log("----------------------");
}

// Bài 3: Tìm số nhỏ nhất
function TimSoNNhat(){
    console.log("Bài 3 - Tìm số nhỏ nhất");  
    var minArrb3 = ArrN[0];
    for(var i = 0; i < ArrN.length; i++){
        var mtn = ArrN[i];
        if(mtn < minArrb3){
            minArrb3 = mtn;
        }
    }
    console.log('Số nhỏ nhất: ', minArrb3);
    document.getElementById("KQ_3").innerHTML = 
    `<h2>Số nhỏ nhất: ${minArrb3}</h2>`
    console.log("----------------------");
}

// Bài 4: Tìm số dương bé nhất
function TimSoDNNhat(){
    console.log("Bài 4 - Tìm số dương nhỏ nhất");
    var dem = 0;

    for(var i = 0; i < ArrN.length; i++){
        var mtn = ArrN[i];
        if(mtn > 0){
            var minArrb4 = mtn;
            dem ++;
            break;
        }
    }
    for(var i = 0; i < ArrN.length; i++){
        var mtn = ArrN[i];
        if(mtn > 0 && minArrb4 > mtn){
            minArrb4 = mtn;
        }
    }

    if(dem == 0){
        console.log("không có số dương trong mảng");
        document.getElementById("KQ_4").innerHTML = 
        `<h2>không có số dương trong mảng</h2>`
    }else{
        console.log("số dương nhỏ nhất:", minArrb4);
        document.getElementById("KQ_4").innerHTML = 
        `<h2>Số dương nhỏ nhất: ${minArrb4}</h2>`
    }
    
    console.log("----------------------");
}

// Bài 5: Tìm số chẵn cuối cùng
function TimSoChanCCung(){
    console.log("Bài 5 - Tìm số chẵn cuối cùng");
    var ChanArrb5 = "không có số chẵn cuối cùng";

    for(var i = 0; i < ArrN.length; i++){
        var mtn = ArrN[i];
        if(mtn % 2 == 0){
            var ChanArrb5 = mtn;
        }
    }

    console.log("số chẵn cuối cùng:", ChanArrb5);
    document.getElementById("KQ_5").innerHTML = 
    `<h2>Số chẵn cuối cùng: ${ChanArrb5}</h2>`

    console.log("----------------------");
}

// Bài 6: Đổi chỗ
function DoiCho(){
    console.log("Bài 6 - Đổi chỗ");
    var vt1, vt2 = null;
    vt1 = document.getElementById("txtvt1B6").value * 1;
    // document.getElementById("txtvt1B6").value = "";
    vt2 = document.getElementById("txtvt2B6").value * 1;
    // document.getElementById("txtvt2B6").value = "";
    var tam = null;
    tam = ArrN[vt1];
    ArrN[vt1] = ArrN[vt2];
    ArrN[vt2] = tam;

    document.getElementById("KQ_6").innerHTML = 
    `<h2>Mảng sau khi đổi vị trí: ${ArrN}</h2>`
    console.log("Mảng sau khi đổi vị trí:", ArrN);
    console.log("----------------------");
    
}

// Bài7 : Sắp Xếp tăng dần
function SapXepTangDan(){
    console.log("Bài 7 - Sắp xếp tăng dần");
    for(var i = 0;i<ArrN.length-1;i++){
        for(var j = i + 1;j<ArrN.length;j++){
            if(ArrN[i]>ArrN[j]){
                var tam = ArrN[j];
                ArrN[j] = ArrN[i];
                ArrN[i] = tam;
            }
        }
    }

    document.getElementById("KQ_7").innerHTML = 
    `<h2>Mảng sau khi sắp xếp: ${ArrN}</h2>`
    console.log("Mảng sau khi sắp xếp:", ArrN);
    console.log("----------------------");
}

// Bài 8: Tìm số nguyên tố đầu tiên
function TimSoNguyenTo(){
    console.log("Bài 8 - Tìm số nguyên tố đầu tiên");
    
    var i;

    for(var t = 0;t<ArrN.length;t++){
        n = ArrN[t];
        var kt = 0;
        if(n<2){
            kt++;
        }
        for(i = 2;i <= Math.sqrt(n); i++){
            if(n % i == 0){
                kt++;
            }
        }
        if(kt == 0){
            console.log(n,"là số nguyên tố.");
            document.getElementById("KQ_8").innerHTML = 
            `<h2>Số nguyên tố đầu tiên là: ${n}</h2>`
            console.log("Số nguyên tố đầu tiên là:", n);
            break;
        }else{
            console.log(n,"không là số nguyên tố.");
        }
    }
    console.log("----------------------");
}


// Bài 9: Đếm số nguyên
var ArrThuc = [];
function ThemSoNT(){
    console.log("Bài 9_1 - Thêm số thực");

    var t = document.getElementById("txtsothuc").value * 1;
    document.getElementById("txtsothuc").value = "";
    
    ArrThuc.push(t);
    console.log('Mảng n: ', ArrThuc);

    document.getElementById("KQ_9_1").innerHTML = 
    `<h2>Mảng: ${ArrThuc}</h2>`

    console.log("----------------------");
}
function DemSoNguyen(){
    console.log("Bài 9_2 - Đếm số nguyên");
    var dem = 0;
    for(var i = 0;i<ArrThuc.length;i++){
        var mt = ArrThuc[i];
        if(Number.isInteger(mt) == true){
            // console.log(mt, "là số nguyên.");
            dem ++;
        }else{
            // console.log(mt, "không là số nguyên.");
        }
    }

    console.log("Số nguyên:", dem);
    document.getElementById("KQ_9_2").innerHTML = 
    `<h2>Số nguyên: ${dem}</h2>`

    console.log("----------------------");
}

//Bài 10: So sánh số lượng số âm và dương
function SoSanhAmDuong(){
    console.log("Bài 10 - So sánh số lượng số âm và dương");

    var demduong = 0;
    var demam = 0;

    for(var i = 0; i<ArrN.length; i++){
        var son = ArrN[i];
        if(son > 0){
            demduong ++;
        }else if(son < 0){
            demam ++;
        }
    }
    console.log('demduong: ', demduong);
    console.log('demam: ', demam);

    if(demduong > demam){
        console.log("số dương > số âm");
        document.getElementById("KQ_10").innerHTML = 
        `<h2>Số dương > Số âm</h2>`
    }else if(demduong == demam){
        console.log("số dương = số âm");
        document.getElementById("KQ_10").innerHTML = 
        `<h2>Số dương = Số âm</h2>`
    }else{
        console.log("số dương < số âm");
        document.getElementById("KQ_10").innerHTML = 
        `<h2>Số dương < Số âm</h2>`
    }


    console.log("----------------------");
}